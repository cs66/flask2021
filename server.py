from flask import Flask, render_template,request
import json
app = Flask(__name__)
world = json.load(open('worldl.json'))

@app.route("/")
def index():
    return "Andrew was here!"
    
@app.route("/country/<int:n>",methods=['GET','DELETE','PUT'])
def country(n):
    global world
    target = [c for c in world if c['id']==n]
    if len(target) == 1:
        if request.method == 'GET':
            return render_template('index.html', country=target[0])
        if request.method == 'DELETE':
            world = [x for x in world if x['id'] != n]
            return "That's sad"
        if request.method == 'PUT':
            j = request.json
            target[0]['population'] = j['population']
            return "not doing that"
    else:
        return "None", 404


if __name__ == '__main__':
    app.run(debug=True)

