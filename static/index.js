document.getElementById('delete').onclick = (e)=>{
    fetch(`/country/${e.target.dataset.cn}`,{method:'DELETE'})
}

document.getElementById('update').onclick = (e)=>{
    let payload = {population:parseInt(document.getElementById('population').value)};
    fetch(`/country/${e.target.dataset.cn}`,
        {
            method:'PUT',
            body:JSON.stringify(payload),
            headers: {
            'Content-Type': 'application/json',
            },
        }
    )
}

